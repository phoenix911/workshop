package com.base.common.util;


import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Title   字符串 正则处理
 * @Author 覃忠君 on 2017/3/28.
 * @Copyright © 长笛龙吟
 */
public class StringRegexUtils {
    //加密手机正则
    private static final String ENCRYPT_MOBILE_REGEX = "(\\d{3})\\d{4}(\\d{4})";
    //加密身份证18位正则
    private static final String ENCRYPT_IdNumber_REGEX_18 = "(\\d{3})\\d{11}(\\d{3})([0-9]|X|x)";
    //加密身份证15位正则
    private static final String ENCRYPT_IdNumber_REGEX_15 = "(\\d{3})\\d{8}(\\d{4})";
    //银行卡正则
    private static final String ENCRYPT_BANK_CARD = "/^(\\d{4})(\\d{4})(\\d{4})$/";
    //加密输出值正则
    public static final String ENCRYPT_VALUE = "$1****$2";
    /** 静态文件资源 */
    public static final String STATIC_RESOURCE_SUFFIX = ".*(css|jpg|png|gif|js|ico|jpeg)";
    /** 图片资源后缀 */
    public static final String STATIC_IMAGE_SUFFIX = ".*(jpg|png|gif|ico|jpeg|bmp)";

    /** 特殊字符 */
    public static final String PARTICULAR_CHARACTER_REGEX = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。°，、？]";

    /**
     * 是否存在特殊字符
     * @param str
     * @return
     */
    public static boolean isParticular(String str){
        Pattern p = Pattern.compile(PARTICULAR_CHARACTER_REGEX);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * 加密银行卡号
     * @param value
     * @return
     */
    public static String encryptBankcard(String value){
        if(StringUtils.isBlank(value)){
            return value;
        }
        return value.replaceAll(ENCRYPT_BANK_CARD, ENCRYPT_VALUE);
    }

    /**
     * 加密手机号
     * @param value
     * @return
     */
    public static String encryptMobile(String value){
        if(StringUtils.isBlank(value)){
            return value;
        }
        return value.replaceAll(ENCRYPT_MOBILE_REGEX, ENCRYPT_VALUE);
    }

    /**
     * 加密身份证
     * @param value
     * @return
     */
    public static String encryptIdNumber(String value){
        if(StringUtils.isBlank(value)){
            return value;
        }

        if(15 == value.length()) {
            return value.replaceAll(ENCRYPT_IdNumber_REGEX_15,"$1***********$2");
        }
        return value.replaceAll(ENCRYPT_IdNumber_REGEX_18,"$1***********$2$3");
    }


    /**
     * 是否为静态资源
     * @param url
     * @return  true  是  false 否
     */
    public static boolean isStaticResource(String url){
        return url.matches(STATIC_RESOURCE_SUFFIX);
    }

    /**
     * 是否为静态Image资源
     * @param path
     * @return  true  是  false 否
     */
    public static boolean isImageResource(String path){
        return path.matches(STATIC_IMAGE_SUFFIX);
    }
}

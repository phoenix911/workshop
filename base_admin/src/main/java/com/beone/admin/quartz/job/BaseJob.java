package com.beone.admin.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @title  quartz 任务抽象
 * @Author 覃球球
 * @Version 1.0 on 2018/10/26.
 * @Copyright 贝旺科技
 */
public abstract class BaseJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        runTask();
    }

    /**
     * 执行任务
     */
    public abstract void runTask();
}

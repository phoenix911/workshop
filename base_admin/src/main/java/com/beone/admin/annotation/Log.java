package com.beone.admin.annotation;

import java.lang.annotation.*;

/**
 * @title  系统日志注解
 * @Author 覃球球
 * @Version 1.0 on 2018/4/28.
 * @Copyright 长笛龙吟
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
@Inherited
public @interface Log {

    enum LOG_TYPE {ADD, UPDATE, DEL, SAVE, LOGIN, LOGOUT, ATHOR, START, STOP};

    /**
     * 内容
     */
    String desc();

    /**
     * 类型 curd
     */
    LOG_TYPE type() default LOG_TYPE.ATHOR;
}
